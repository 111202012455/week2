package tugas2;

public class Lampu {
	String jenis;  
    boolean lampu;  

    // Konstruktor  
    Lampu() {  
         jenis = "LED";  
         lampu = true;  
    }  

    void NyalakanLampu() {  
         lampu = true;  
         System.out.println("Lampu Hidup");  
    }  

    void MatikanLampu() {  
         lampu = false;  
         System.out.println("Lampu Mati");  
    }  

    public static void main(String[] args) {  
         Lampu refLampu = new Lampu();  

         // Tampilkan masing nilai atribut  
         System.out.println("Jenis : " + refLampu.jenis);
         System.out.println("Boolean : " + refLampu.lampu);  
    }  

}
