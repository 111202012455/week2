package tugas2;

public class AksiSepeda {
	 int rodasepeda;
	 int gear = 5;
	 
	    String jenissepeda, merksepeda;

	    public AksiSepeda(int jumlahRoda, String jenis, String merk) {
	        rodasepeda = jumlahRoda;
	        jenissepeda = jenis;
	        merksepeda = merk;
	        
	    }
	    
	    void ngerem() {
	    	System.out.println("Sepeda Sedang di Rem");
	    }
	    void maju() {
	    	System.out.println("Sepeda Sedang berjalan");
	    }


		public static void main(String[] args) {
		    AksiSepeda sepeda= new AksiSepeda(2,"Gunung","Honda");
		    System.out.println("Sepeda "+sepeda.jenissepeda+" Bermerk "+sepeda.merksepeda+" memiliki Jumlah Roda "+sepeda.rodasepeda);
		    
		    int gearSepeda = sepeda.gear;
		    System.out.println("Jumlah Gear "+gearSepeda);
		    sepeda.ngerem();
		}
}


/// Nama 	: Farid Choirul Burhan
/// NIM 	: A11.2020.12455