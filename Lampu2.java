package tugas2;

public class Lampu2 {
	String jenis;  
    boolean lampu;  

    // Konstruktor  
    Lampu2() {  
         jenis = "LED";  
         lampu = false;  
    }  

    void NyalakanLampu() {  
         lampu = true;  
         System.out.println("Lampu Hidup");  
    }  

    void MatikanLampu() {  
         lampu = false;  
         System.out.println("Lampu Mati");  
    }  

    public static void main(String[] args) {  
         Lampu2 refLampu = new Lampu2();  

         // Tampilkan masing nilai atribut  
         System.out.println("Jenis : " + refLampu.jenis);
         System.out.println("Boolean : " + refLampu.lampu);  
    }  

}
